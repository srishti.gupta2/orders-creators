from pymongo import MongoClient
import pandas as pd

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient('mongodb://dl-user-1:A2mClcihXGlNQC7M@datalake0-3comg.a.query.mongodb.net/Database0?ssl=true&authSource=admin')
result = client['Database0']['Order_Commission_Invoice_4'].aggregate([
    {
        '$match': {
            'payment_status.code': 'success'
        }
    }, {
        '$project': {
            '_id': 0, 
            'Order Date': {
                '$dateToString': {
                    'format': '%d-%m-%Y', 
                    'date': '$created_at', 
                    'timezone': 'Asia/Calcutta'
                }
            }, 
            'Order ID': '$order_id', 
            'Brand Name': '$brand_info.name', 
            'Quantity': '$items.quantity', 
            'Shipping Display Name': '$invoice_info.shipping_address.display_name', 
            'User Account Name': '$user_info.full_name', 
            'Payment Mode': {
                '$cond': {
                    'if': {
                        '$eq': [
                            '$payment_mode', 'cod'
                        ]
                    }, 
                    'then': 'cod', 
                    'else': 'prepaid'
                }
            }, 
            'Grand Total': '$grand_total.value', 
            'Creator Name': '$items.influencer_name', 
            'Creator Username': '$items.influencer_username', 
            'Store Link': '$items.store_link', 
            'Creator Commission Value': '$items.commission_info.commission_value', 
            'Product Name': '$items.catalog_info.name', 
            'Discount Coupon Code': '$coupon_info.code', 
            'Offer Value': '$items.offer_value.value', 
            'Final Selling Price': {
                '$subtract': [
                    '$items.total_price.value', {
                        '$max': [
                            '$items.offer_value.value', 0
                        ]
                    }
                ]
            }, 
            'Total Price': '$items.total_price.value', 
            'Current Order Status': '$order_status.code'
        }
    }
])
df = pd.DataFrame(result)
df.to_csv("orders&creators.csv")